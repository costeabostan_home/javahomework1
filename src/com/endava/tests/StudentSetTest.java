package com.endava.tests;


import com.endava.Student;
import com.endava.StudentSet;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertTrue;

class StudentSetTest {

    @Test
    void sizeTest() {
        LocalDate date = LocalDate.now();
        Student student = new Student("Test",date,"test");

        StudentSet studentSet = new StudentSet();
        studentSet.add(student);
        assertTrue(studentSet.size()==1);
    }
}