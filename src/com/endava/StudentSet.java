package com.endava;

import java.util.*;

public class StudentSet implements Set<Student> {

    public LinkedList<Student> studentsList = new LinkedList<>();

    @Override
    public int size() {
        return studentsList.size();
    }

    @Override
    public boolean isEmpty() {
        return studentsList.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        //TODO
        return false;
    }

    @Override
    public Iterator<Student> iterator() {
        return studentsList.iterator();
    }

    @Override
    public Object[] toArray() {
        return studentsList.toArray();
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean add(Student student) {
        return studentsList.add(student);
    }

    @Override
    public boolean remove(Object o) {
        return studentsList.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return studentsList.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends Student> collection) {
        return studentsList.addAll(collection);
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        //TODO
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        //TODO
        return false;
    }

    @Override
    public void clear() {
        studentsList.clear();
    }

    @Override
    public Spliterator<Student> spliterator() {
        return studentsList.spliterator();
    }
}

