package com.endava;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
	// write your code here
        LocalDate date = LocalDate.now();
        Student student = new Student("Test",date,"test");

        StudentSet studentSet = new StudentSet();
        studentSet.add(student);
    }
}
